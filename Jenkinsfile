pipeline {

    environment {
        REGISTRY_URL = "http://harbor.demo.com/"
        REGISTRY_NAME = "harbor.demo.com/pipeline/shopizer"
        GIT_URL = "http://192.168.100.175/rakesh/shopizer_app_demo.git"
        SONARQUBE_URL = "http://192.168.100.176:9000"
    }

     agent { 
        label 'jnlp'  
    }

    options {
      gitLabConnection("${env.GIT_URL}")
    }

    triggers {
        gitlab(triggerOnPush: true, triggerOnMergeRequest: true, branchFilterType: 'All')
        pollSCM('H/2 * * * *')
        
    }

    tools{
        maven 'default-maven'    
    }

    stages {
        stage('Checkout Source') {
            steps {
                git "${env.GIT_URL}"
                sh 'ls -al'
            }
        }
        stage('Code Quality Check via SonarQube') {
            steps {
                script {
                    def scannerHome = tool 'sonar-scanner';
                    withSonarQubeEnv("sonarqube") {
                        sh "${tool("sonar-scanner")}/bin/sonar-scanner \
                        -Dsonar.projectKey=shopizer_app_demo\
                        -Dsonar.java.binaries=$WORKSPACE \
                        -Dsonar.host.url='$SONARQUBE_URL' "
                    }
                }
            }
        }
        stage('Maven_Build_&_PushToGitlab') {
            steps {
                sh 'echo $JAVA_HOME'
                sh 'mvn --version'
                sh 'mvn clean install'            }
        }
        stage('Application_ContainerImage_Build_&_Push_Harbor') {
            steps{
                script {
                    withCredentials([usernamePassword( credentialsId: 'harbor', usernameVariable: 'USR', passwordVariable: 'PWD')]) {
                        docker.withRegistry(REGISTRY_URL , 'harbor') {
                            sh "cd ./sm-shop"
                            sh "docker login harbor.demo.com -u ${USR} -p ${PWD}"
                            dockerImage = docker.build REGISTRY_NAME + ":latest"
                            dockerImage.push()
                        } 
                    }
                }
            }
        }
        stage('Deploy App') {
            steps {
                script {
                    kubernetesDeploy(configs: "sm-shop-deploy.yaml", kubeconfigId: "NewUatClusterKubeconfig")
                }
            }
        }
    }
}