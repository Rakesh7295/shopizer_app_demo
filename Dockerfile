FROM tomcat:latest

LABEL maintainer=”shopizerDemo@test.com”

ADD ./sm-shop/target/*.war /usr/local/tomcat/webapps/

EXPOSE 8080
